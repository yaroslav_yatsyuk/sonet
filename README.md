# README.md
![](https://pandao.github.io/editor.md/images/logos/editormd-logo-180x180.png)

#####How build and run Spring Boot App?
- Option 1 (recommended):
`$ mvn package`  - build  JAR;
`$ mvn -q spring-boot:run -Dspring.profiles.active=dev`  - run app using `dev` property [can be `dev` or `prod` ];
- Option 2:
` $ mvn package ` -bild JAR;
` $ java -jar -Dspring.profiles.active=dev jName.jar` - - run jar using `dev` property [can be `dev` or `prod` ];
- Option 3 (NOT recommended):
`spring.profiles.active=dev` - write it in application.properties and run project;
#####How logging works?
We use standart logging system of Spring Boot. We split logging on 'dev' and on 'prod'. In 'prod' mode we write ERRORS logs into folder ./logs and console. In 'dev' mode we write INFO only into console.

#####Spring boot admin
- Link: http://admin.sonet-social.net
- Repo: https://bitbucket.org/KolisnykSerhii/sonet-admin/src/master/
