package com.softserve.ita.sonet.model.vote;

import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.model.Poll;
import com.softserve.ita.sonet.model.Choice;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.Objects;


@NoArgsConstructor
@Getter
@Setter
@Entity
@IdClass(VotePrimaryKey.class)
@Table(name = "votes")
public class Vote {

    @Id
    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    @Id
    @ManyToOne
    @JoinColumn(name = "poll_id", insertable = false, updatable = false)
    private Poll poll;

    @ManyToOne
    @JoinColumn(
            name = "сhoice_id",
            referencedColumnName = "id"
    )
    private Choice choice;


}
