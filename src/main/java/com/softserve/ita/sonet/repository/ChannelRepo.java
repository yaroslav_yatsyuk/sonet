package com.softserve.ita.sonet.repository;

import com.softserve.ita.sonet.model.Channel;
import com.softserve.ita.sonet.model.Chat;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChannelRepo extends JpaRepository<Channel, Long> {

    boolean existsByChatAndName(Chat chat, String name) throws DataAccessException;
}
