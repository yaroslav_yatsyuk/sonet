package com.softserve.ita.sonet.repository;

import com.softserve.ita.sonet.model.like.Like;
import com.softserve.ita.sonet.model.like.LikePrimaryKey;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface LikeRepository extends JpaRepository<Like, LikePrimaryKey> {

    List<Like> findAllByPostId(Long postId);

}
