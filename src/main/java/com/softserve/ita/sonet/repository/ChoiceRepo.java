package com.softserve.ita.sonet.repository;

import com.softserve.ita.sonet.model.Choice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ChoiceRepo extends JpaRepository<Choice, Long> {

}
