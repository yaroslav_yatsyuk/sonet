package com.softserve.ita.sonet.utils;

import com.softserve.ita.sonet.dto.model.ChannelDTO;
import com.softserve.ita.sonet.model.Channel;
import org.springframework.stereotype.Component;

@Component
public class ChannelMapper implements CustomModelMapper<Channel, ChannelDTO> {
    @Override
    public ChannelDTO toDTO(Channel channel) {
        return new ChannelDTO(channel.getId(), channel.getChat().getId(), channel.getName(), channel.getType());
    }
}
