package com.softserve.ita.sonet.utils;

import com.softserve.ita.sonet.dto.response.FollowUserResponse;
import com.softserve.ita.sonet.model.follow.user.UserFollow;
import org.springframework.stereotype.Component;

@Component
public class FollowUserMapper implements CustomModelMapper<UserFollow, FollowUserResponse> {

    @Override
    public FollowUserResponse toDTO(UserFollow entity) {
        FollowUserResponse followUserResponse = new FollowUserResponse();
        followUserResponse.setCreationTime(entity.getCreationTime());
        followUserResponse.setUpdateTime(entity.getUpdateTime());
        followUserResponse.setFollowerId(entity.getFollowerId());
        followUserResponse.setFollowingId(entity.getFollowingId());

        return followUserResponse;
    }
}
