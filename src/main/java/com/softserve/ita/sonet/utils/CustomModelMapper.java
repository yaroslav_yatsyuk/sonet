package com.softserve.ita.sonet.utils;

import com.softserve.ita.sonet.dto.model.DTO;
import com.softserve.ita.sonet.model.entity.BaseEntity;

public interface CustomModelMapper<T extends BaseEntity, D extends DTO> {

      D toDTO(T entity);
}
