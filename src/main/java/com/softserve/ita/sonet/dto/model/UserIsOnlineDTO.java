package com.softserve.ita.sonet.dto.model;

import java.time.LocalDateTime;

public class UserIsOnlineDTO {

    private Boolean isOnline;

    private LocalDateTime lastActivity;
}
