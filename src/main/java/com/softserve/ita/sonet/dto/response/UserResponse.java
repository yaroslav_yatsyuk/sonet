package com.softserve.ita.sonet.dto.response;

import com.softserve.ita.sonet.dto.model.DTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse implements DTO {

    private Long id;

    private String firstName;

    private String lastName;

    private String avatar;

}
