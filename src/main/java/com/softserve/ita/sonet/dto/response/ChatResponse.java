package com.softserve.ita.sonet.dto.response;

import com.softserve.ita.sonet.dto.model.DTO;
import com.softserve.ita.sonet.dto.model.MessageDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatResponse implements DTO {

    private Long id;

    private String title;

    private MessageDTO lastMessage;

}
