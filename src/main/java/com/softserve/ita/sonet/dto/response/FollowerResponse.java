package com.softserve.ita.sonet.dto.response;

import com.softserve.ita.sonet.model.Image;
import lombok.Data;

@Data
public class FollowerResponse {

    private Long id;

    private String nickname;

    private String firstName;

    private String lastName;

    private Image avatar;

}