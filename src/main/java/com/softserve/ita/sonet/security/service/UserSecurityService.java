package com.softserve.ita.sonet.security.service;

import com.softserve.ita.sonet.exception.EmailSendingException;
import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.security.dto.RegisterUserRequest;
import com.softserve.ita.sonet.security.dto.SecuredUserDTO;

public interface UserSecurityService {

    User register(RegisterUserRequest requestUser);

    SecuredUserDTO findByEmail(String username);

    SecuredUserDTO findById(Long id);

    boolean verifyEmailToken(String token);

    boolean resetPasswordRequest(String email) throws EmailSendingException;

    boolean resetPassword(String token, String password) throws EmailSendingException;

    boolean isActive(String email);
}
