package com.softserve.ita.sonet.security.service;

import com.softserve.ita.sonet.security.dto.FacebookAccessTokenRequest;
import com.softserve.ita.sonet.security.dto.LoginRequest;

public interface FacebookSecurityService {

    LoginRequest authenticateFacebookUser(FacebookAccessTokenRequest facebookRequest);
}
