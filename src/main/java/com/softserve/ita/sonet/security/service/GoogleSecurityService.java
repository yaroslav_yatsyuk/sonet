package com.softserve.ita.sonet.security.service;

import com.softserve.ita.sonet.security.dto.GoogleUserInfo;
import com.softserve.ita.sonet.security.dto.LoginRequest;

public interface GoogleSecurityService {

    LoginRequest authenticateGoogleUser(GoogleUserInfo googleUser);
}
