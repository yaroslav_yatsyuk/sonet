package com.softserve.ita.sonet.service.impl;

import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.Choice;
import com.softserve.ita.sonet.repository.ChoiceRepo;
import com.softserve.ita.sonet.service.ChoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChoiceServiceImpl implements ChoiceService {

    private ChoiceRepo choiceRepo;

    @Autowired
    public ChoiceServiceImpl(ChoiceRepo choiceRepo) {
        this.choiceRepo = choiceRepo;
    }

    @Override
    public Choice add(String description) {
        Choice choice = new Choice();
        choice.setText(description);
        choice = choiceRepo.save(choice);
        return choice;
    }

    @Override
    public Choice findById(Long id) throws EntityNotFoundException {
        return choiceRepo.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Choice with id:{0} not found", id));
    }

}
