package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.request.PollAddRequest;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.Poll;

import java.util.List;

public interface PollService {

    Poll create (PollAddRequest pollAddRequest);

    Poll findById (Long id) throws EntityNotFoundException;

    void deleteById(Long pollId) throws EntityNotFoundException;

    List<Poll> findAllByPostId(Long postId);

}
