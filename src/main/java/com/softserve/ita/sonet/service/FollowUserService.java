package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.model.FollowsDTO;
import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.model.follow.user.UserFollow;

import java.util.List;

public interface FollowUserService {

    UserFollow follow(FollowsDTO followsDTO);

    void unfollow(FollowsDTO followsDTO);

    List<User> getFollowings(Long followerId);

    List<User> getFollowers(Long followingId);

    boolean checkIfFollowed(Long followerId, Long followingId);

}
