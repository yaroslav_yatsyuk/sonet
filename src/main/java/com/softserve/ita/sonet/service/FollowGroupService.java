package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.request.FollowToGroupRequest;
import com.softserve.ita.sonet.model.follow.group.FollowGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface FollowGroupService {

    FollowGroup follow(FollowToGroupRequest followRequest);

    void unfollow(Long userId, Long groupId);

    Page<FollowGroup> getFollowsByGroupId(Long groupId, Pageable pageable);

    Page<FollowGroup> getFollowsByFollowerId(Long userId, Pageable pageable);

    boolean isFollowed(Long followerId, Long groupId);

}
