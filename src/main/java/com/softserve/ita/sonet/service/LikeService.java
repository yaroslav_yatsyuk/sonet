package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.request.CreateLikeRequest;
import com.softserve.ita.sonet.model.like.Like;

import java.util.List;

public interface LikeService {

    void deleteLikeByUserIdAndPostId(Long postId, Long userId);

    Like findLikeByPostIdAndUserId(Long postId, Long userId);

    Like insertLike(CreateLikeRequest request);

    List<Like> findAllByPostId(Long id);
}
