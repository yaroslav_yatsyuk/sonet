package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.request.UpdateLocationRequest;
import com.softserve.ita.sonet.model.UserLocation;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LocationService {

    UserLocation updateLocationInfo(UpdateLocationRequest request);

    List<UserLocation> getUsersNearMe(Long userId, Pageable pageable);

    double calculateDistanceBetweenTwoLocations(
            double currentUserLatitude,
            double anotherUserLatitude,
            double currentUserLongitude,
            double anotherUserLongitude
    );

}
