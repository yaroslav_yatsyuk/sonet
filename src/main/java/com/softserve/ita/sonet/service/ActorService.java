package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.model.Actor;

public interface ActorService {

    Actor findById(Long id);

    boolean existsByNickname(String nickname);

    Actor save(Actor actor);
}
