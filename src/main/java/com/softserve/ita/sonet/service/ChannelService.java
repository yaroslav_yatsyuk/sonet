package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.model.ChannelDTO;
import com.softserve.ita.sonet.model.Channel;
import com.softserve.ita.sonet.model.content.Message;

import java.util.List;

public interface ChannelService {

    Channel create(ChannelDTO channelRequest);

    Channel save(Channel channel);

    List<Message> getAllMessages(Long channelId);

    Channel getChannelById(Long channelId);
}
