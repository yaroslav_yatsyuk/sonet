package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.request.CreateChatRequest;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.Channel;
import com.softserve.ita.sonet.model.Chat;

import java.util.List;


public interface ChatService {

  Chat create(CreateChatRequest chat);

  void delete(Long chatId);

  Chat getChatById(Long chatId) throws EntityNotFoundException;

  List<Channel> getAllChannels(Long chatId);
}
