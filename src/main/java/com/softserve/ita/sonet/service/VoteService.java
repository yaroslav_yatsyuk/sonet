package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.request.VoteAddRequest;
import com.softserve.ita.sonet.dto.response.VoteAddResponse;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.vote.Vote;

public interface VoteService {

    Vote saveVote(VoteAddRequest voteAddRequest);

    void deleteById(Long userId, Long pollId) throws EntityNotFoundException;

    VoteAddResponse vote (VoteAddRequest voteAddRequest);

    Boolean isChoiceExist (Long userId, Long pollId);

    VoteAddResponse findVoteByUserIdAndPollId(Long userId, Long pollId);

}
