package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.Choice;

public interface ChoiceService {

    Choice add(String description);

    Choice findById(Long id) throws EntityNotFoundException;

}
