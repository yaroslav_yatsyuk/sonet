package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.model.CommentDTO;
import com.softserve.ita.sonet.dto.request.CreateCommentRequest;
import com.softserve.ita.sonet.model.content.Comment;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CommentService {

    List<Comment> findAllCommentsByPostId(Long postId, Pageable paging);

    List<Comment> findAllCommentsByPostIdAndCreatorId(Long postId, Long userId);

    List<Comment> findAll();

    Comment findCommentById(Long id);

    Comment updateComment(Long id, CommentDTO commentToUpdate);

    void deleteCommentById(Long id);

    Comment saveComment(CreateCommentRequest request);

}
