package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.model.MessageDTO;
import com.softserve.ita.sonet.model.Channel;
import com.softserve.ita.sonet.model.content.Message;

public interface MessageService {

    Message save(Long id, MessageDTO messageDTO);

    Message getTop1ByChannelOrderByCreationTimeDesc(Channel channel);

}
